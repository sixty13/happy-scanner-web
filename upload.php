<?php
// header('Content-type: application/json');

/* Getting file name */
$filename = $_FILES['file']['name'];

/* Location */
$location = "img/".$filename;
$imageFileType = pathinfo($location,PATHINFO_EXTENSION);

/* Valid Extensions */
$valid_extensions = array("jpg","jpeg","png");

/* Check file extension */
if( in_array(strtolower($imageFileType),$valid_extensions) )
{
    /* Upload file */
    if(move_uploaded_file($_FILES['file']['tmp_name'],$location))
        echo $location;//return json_encode( ['message' => 'Profile picture updated successfully!', 'fileName' => $location] );
    else
        echo "205";//return json_encode( ['message' => 'Profile picture not updated yet, please try again!', 'fileName' => ""] );
}
else
    echo "204";//return json_encode( ['message' => 'Profile picture not updated yet, please try again!', 'fileName' => ""] );
?>