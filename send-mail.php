<?php
error_reporting(-1);
ini_set('display_errors', 'On');
set_error_handler("var_dump");

$msg = 'Here is your beautiful selfie taken at Tamira life, Chennai - Click here to view the photo ';
$image = ( isset( $_GET['image'] ) ) ? "https://bootstrapmonster.com/core/share-image/".$_GET['image'] : "https://bootstrapmonster.com/core/share-image/img/rt1-cardiovascular-surgery.jpg";
$email = ( isset( $_GET['email'] ) ) ? $_GET['email'] : 'kakdiya.gautam288@gmail.com';

$content = "";
$content.= '<html>
    <head>
        <title>Firebase share image</title>
        <meta charset="utf-8"> 
        <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1"> 
        <meta name="viewport" content="width=device-width, initial-scale=1"> 
        <meta name="description" content="Tamira Firebase Share Image"> 
        <meta name="author" content="Bootstrapmonster">
        
        <meta property="og:url"           content="http://bootstrapmonster.com/core/share-image" />
        <meta property="og:type"          content="website" />
        <meta property="og:title"         content="Tamira Firebase Share Image" />
        <meta property="og:description"   content="Tamira Firebase Share Image" />
        <meta property="og:image"         content="'.$image.'" />

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <style>
            .fa { font-size: 25px; width: 45px; text-align: center; text-decoration: none; border-radius: 50%; padding: 10px; }
            a:hover { color: snow !important; text-decoration: none !important; }
            .fa:hover { opacity: 0.7; }
            .fa-facebook { background: #3B5998; color: white; }
            .fa-twitter { background: #55ACEE; color: white; }
            .fa-instagram { background: #ad4d4d; color: white; }
            .fa-whatsapp { background: #04ab3f; color: white; }
            .fa-envelope { background: #8904ab; color: white; }
        </style>
        
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
        <script src="https://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center p-5">
                    <img src="'.$image.'" class="img-fluid">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center justify-content-space-around p-2">
                    <p>Dear Customer,</p>
                    <p>Thanks so much for shopping at <b>style one</b></p>
                    <p>we hope you had nice capture experience and looking forward to you visit us again</p>
                    <br>
                    <p>Thanks & Regards,</p>
                    <p>Bilimbe Team Chennai</p>
                </div>
            </div>
        </div>
    </body>
</html>';
 
$header = "From: work@sixty13.com\r\n";
$header.= "MIME-Version: 1.0\r\n";
$header.= "Content-Type: text/html; charset=ISO-8859-1\r\n";
$header.= "X-Priority: 1\r\n";

$status = mail($email, "Tamira Firebase Share Image", $content, $header);

if($status)
    echo 'Your mail has been sent!';
else
    echo 'Something went wrong. Please try again!';