<html>
    <head>
        <title>Your Beautiful Selfie is here</title>
        <meta charset="utf-8"> 
        <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1"> 
        <meta name="viewport" content="width=device-width, initial-scale=1"> 
        <meta name="description" content="Share Image"> 
        <meta name="author" content="Bootstrapmonster">
        
        <?php
            $img_url = ( isset( $_GET['image'] ) ) ? "http://tamira.peacockindia.in/".$_GET['image'] : "http://tamira.peacockindia.in/img/rt1-cardiovascular-surgery.jpg";
            // $file = 'database/short-link.txt';

            // $content = file( $file );

            // if( !empty( $content ) )
            // {
            //     foreach( $content as $url )
            //     {
            //         $short = explode( "|", trim( $url ) );

            //         if ($short[1] == $_GET['q'] ) {
            //             $img_url = $short[0];
            //             break;
            //         }
            //     }
            // }
                
            // $img_url = ( isset( $_GET['image'] ) ) ? "http://tamira.peacockindia.in/share-image/".$_GET['image'] : "http://tamira.peacockindia.in/img/rt1-cardiovascular-surgery.jpg";
            $share_link = ( isset( $_GET['image'] ) ) ? "http://tamira.peacockindia.in?image=".$_GET['image'] : "http://tamira.peacockindia.in?image=img/rt1-cardiovascular-surgery.jpg";
            
            $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.short.io/links",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode(array(
                'originalURL' => $share_link,
                'domain' => 'selfies.link'
            )),
            CURLOPT_HTTPHEADER => array(
                "authorization: 1CDbRgS3NiGKeT7kt5RbH9fWdZMQa9NO",
                "content-type: application/json"
            ),
            ));

            $randomString = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            // if ($err) {
            //   echo "cURL Error #:" . $err;
            // } else {
            $randomString = json_decode($randomString);
        ?>
        <meta property="og:url"           content="http://tamira.peacockindia.in/" />
        <meta property="og:type"          content="website" />
        <meta property="og:title"         content="Your Beautiful Selfie is here" />
        <meta property="og:description"   content="Your Beautiful Selfie is here" />
        <meta property="og:image"         content="<?php echo $img_url; ?>" />

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <style>
            .fa { font-size: 25px; width: 45px; text-align: center; text-decoration: none; border-radius: 50%; padding: 10px; }
            a:hover { color: snow !important; text-decoration: none !important; }
            .fa:hover { opacity: 0.7; }
            .fa-facebook { background: #3B5998; color: white; }
            .fa-twitter { background: #55ACEE; color: white; }
            .fa-instagram { background: #ad4d4d; color: white; }
            .fa-whatsapp { background: #04ab3f; color: white; }
            .fa-envelope { background: #8904ab; color: white; }
            .fa-download { background: #ffc107; color: white; }
        </style>
        
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
        <script src="https://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center p-5">
                    <img src="<?php echo $img_url; ?>" class="img-fluid">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center justify-content-space-around p-2">
                    <span>Share with</span>
                </div>
                <div class="col-md-12 text-center justify-content-space-around pb-5">
                    <div>
                        <a href="https://www.instagram.com/?url=https://www.drdrop.co/" target="_blank" rel="noopener" class="fa fa-instagram" target="_blank"></a>
                        <a href="https://api.whatsapp.com/send?text=Thanks for sharing your feedback - here is your beautiful selfie taken at tamira, chennai - click here to view the photo <?php echo $randomString->shortURL; ?>" class="fa fa-whatsapp" target="_blank"></a>
                        <a href="https://twitter.com/share?text=here is your beautiful selfie taken at tamira, chennai - click here to view the photo <?php echo $randomString->shortURL; ?>" class="fa fa-twitter" target="_blank"></a>
                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $randomString->shortURL; ?>&name=Here is your beautiful selfie taken at Tamira life, Chennai&description=Here is your beautiful selfie taken at Tamira life, Chennai" class="fa fa-facebook" target="_blank"></a>
                        <a href="mailto:?subject=Here is your beautiful selfie taken at Tamira life, Chennai&body=Thanks for sharing your feedback - here is your beautiful selfie taken at tamira, chennai - click here to view the photo <?php echo $randomString->shortURL; ?>" class="fa fa-envelope" target="_blank"></a>
                        <a href="<?php echo $img_url; ?>" download="<?php echo $img_url; ?>" class="fa fa-download"></a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>